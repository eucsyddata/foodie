# About Foodie #
Foodie is a demo of a cross-platform RecipeApp in a Xamarin.Form project.
It is based on a Pluralsight-video: [Moving Beyond the Basics with Xamarin Forms](https://app.pluralsight.com/library/courses/xamarin-forms-moving-beyond-basics/table-of-contents) by Matthew Soucoup.

## Part 1 - TableViews ##

A simple Edit form with a TableView and some controls binded to a Recipe object.

### 1.Tableviews ###
Setting up a TableView and Built-in cells


